<?php
// is a good idea to add this two lines in development
error_reporting(E_ALL);
ini_set('display_errors', 'On');

$yii='/yii/yii.php';
// you can also use paths like this one
// $yii="c:/anyplace/yii/framework/yii.php";

$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();
