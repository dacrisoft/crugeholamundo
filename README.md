CREANDO UNA APP CON CRUGE USANDO GIT
====================================

En este codigo que has descargado aqui veras que la carpeta protected/modules no existe, tampoco existe cruge
deberas copiarla o clonarla tu mismo en tu proyecto instalado.

supongamos que tienes estos requisitos de directorio:
~~~
  // tu instalacion de yii esta aqui:
  /e/apps/yii
  // quieres tu aplicacion demo instalada aqui:
  /e/code/crugeholamundo
~~~

1) descarga esta aplicacion completa a:
~~~
// si deseas instalar via descarga, entonces busca el link "download"
// navegas a: "http://bitbucket.org/christiansalazarh/crugeholamundo"
// simplemente lo descargas dentro de:
/e/code/crugeholamundo

// mejor via: usando GIT seria asi:
cd e
cd code
git clone https://bitbucket.org/christiansalazarh/crugeholamundo.git
~~~

2) dale los permisos 775 a los directorios "assets" y "protected/runtime", crea los directorios si no existen.
~~~ 
cd /e/code/crugeholamundo
mkdir assets
chmod 775 assets
cd protected
mkdir runtime
chmod 775 runtime
~~~

3) asegurate de que exista el siguiente directorio:
~~~
/e/code/crugeholamundo/protected/modules
~~~

4) clona cruge:
~~~
cd /e/code/crugeholamundo/protected/modules
git clone https://bitbucket.org/christiansalazarh/cruge.git
~~~

5) tras clonar deberia haberse creado el directorio:
~~~
/e/code/crugeholamundo/protected/modules/cruge
~~~

6) Crea una base de datos llamada 'crugedemo', los parametros de la base de datos los pones 
tu en el archivo /config/main.php de tu aplicacion.

7) instala en tu base de datos al script de cruge
~~~
/e/code/crugeholamundo/protected/modules/cruge/data/ <-- busca aqui el script
~~~

La ayuda oficial de Cruge esta en:

  http://www.yiiframeworkenespanol.org/cruge

